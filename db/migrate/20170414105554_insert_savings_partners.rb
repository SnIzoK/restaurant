class InsertSavingsPartners < ActiveRecord::Migration
  def up
    [
      ["Employer’s", "Workers’ Compensation policies", 496],
      ["Nevada Drug Card", "Discount prescriptions", 175],
      ["Legal Shield", "Pre-paid legal advice 24/7", 444],
      ["United Healthcare", "Healthcare for groups", 120, "per employee"],
      ["Servsafe Food Safety & Alcohol (TAM) Training", "ServSafe Food Safety", 35, "per employee"],
      ["Servsafe Food Safety & Alcohol (TAM) Training", "ServSafe Alcohol Training", 5, "per employee", false],
      ["Fishbowl Marketing", "E-mail Marketing", 480],
      ["Fishbowl Marketing", "E-mail Marketing", 480],
      ["Fishbowl Marketing", "E-mail Marketing", 480],
      ["Broadcast Music, INC", "Broadcast Music Inc - BMI", 327],
      ["Society of European Stage Actors and Composers (SESAC)", "Music Rights Organization", 336],
      ["American Society of Composers, Authors and Publishers (ASCAP)", "Music Rights Organization", 281],
      ["Office Depot  / Office Max", nil, 480, nil, false],
      ["Free Event Tickets for NvRA and NRA", nil, 550, nil, false],
      ["Free Consultation for 20 Minutes", nil, 200, nil, false, "Insurance:http://google.com\r\nLegal:http://twitter.com\r\nLicensing and Permitting:http://facebook.com"]
    ].each_with_index do |item, i|
      h = {
          published: true,
          sorting_position: i + 1,
          image: item[4] != false ? (File.open(Rails.root.join("app/assets/images/logos/rid-logo-#{i + 1}.jpg")) rescue nil) : nil,
          name: item[0],
          short_description: item[1],
          links: item[5],
          saving: item[2],
          saving_subtext: item[3]

      }.keep_if{|k, v| !v.nil? }
      p = SavingsPartner.new
      h.each do |k, v|
        puts "#{i+1}: #{k}"
        p.send("#{k}=", v)
      end
      p.save
    end
  end
end
