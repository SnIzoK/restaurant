class CreateSavingsPartners < ActiveRecord::Migration
  def change
    create_table :savings_partners do |t|
      t.boolean :published
      t.integer :sorting_position

      t.attachment :image

      t.string :name
      t.text :short_description
      t.text :links
      t.string :main_url


      t.integer :saving
      t.string :tooltip_title
      t.text :tooltip_description
      t.string :saving_subtext

      t.timestamps null: false
    end
  end
end
