class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.boolean :published
      t.integer :sorting_position
      t.boolean :show_on_file_a_claim
      t.boolean :show_on_online_billing
      t.attachment :image
      t.string :name
      t.string :address
      t.string :company_site
      t.text :links
      t.text :phones

      t.timestamps null: false
    end
  end
end
