class CreatePolicyChangeRequests < ActiveRecord::Migration
  def change
    create_table :policy_change_requests do |t|
      t.integer :user_id
      t.string :session_id

      t.string :first_name
      t.string :last_name
      t.string :street_address
      t.string :city
      t.string :zip_code
      t.string :state
      t.string :phone
      t.string :email
      t.string :company_name
      t.string :policy_number
      t.date :policy_expiration_date
      t.date :changes_effect_date
      t.text :changes_description

      t.timestamps null: false
    end
  end
end
