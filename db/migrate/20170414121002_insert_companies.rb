class InsertCompanies < ActiveRecord::Migration
  def up
    [
        {show_on_file_a_claim: 't', show_on_online_billing: 't', name: "AmTrust Insurance", address: "59 Maiden Lane, 43rd Floor, New York, NY 10038", company_site: "http://amtrustgroup.com", links: "Member Login:http://google.com", phones: "Phone General:+1 877 528 7878"},
        {show_on_file_a_claim: 't', show_on_online_billing: 't', name: "Philadelphia Insurance Companies", address: "One Bala Plaza, Suite 100, Bala Cynwyd, PA 19004", company_site: "http://phly.com", links: "Claims:http://google.com\r\nService:http://google.com\r\nMake a Payment:http://twitter.com", phones: "Phone Claims:+1 800 682 6671\r\nPhone Service:+1 800 232 3085"},
        {show_on_online_billing: 't', name: "AIG (American International Group, Inc.)", address: "22427 Network Place, Chicago IL 60673-1224", company_site: "http://aig.com", links: "Claims:http://google.com\r\nBilling:http://mail.com", phones: "Phone General:+1 877 528 7878"},
        {show_on_online_billing: 't', name: "Philadelphia Insurance Companies", address: "One Bala Plaza, Suite 100, Bala Cynwyd, PA 19004", company_site: "http://phly.com", links: "Claims:http://google.com\r\nService:http://google.com\r\nMake a Payment:http://twitter.com", phones: "Phone Claims:+1 800 682 6671\r\nPhone Service:+1 800 232 3085"},
    ].each_with_index do |item, i|
      h = {published: 't', sorting_position: i+1, image: File.open(Rails.root.join("app/assets/images/logos/rid-logo-#{i + 1}.jpg"))}.merge(item)
      Company.create(h)
    end
  end
end
