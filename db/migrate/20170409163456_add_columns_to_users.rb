class AddColumnsToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :country
      t.string :language
      t.string :company_name
      t.integer :company_website
      t.integer :full_time_employees
      t.integer :part_time_employees
      t.string :dba
      t.string :phone
      t.string :fax
      t.string :fein
      t.integer :company_ages
      t.integer :payroll_total_annual_restaurant
      t.integer :payroll_total_annual_clerical
      t.integer :payroll_total_annual_sales
      t.integer :payroll_total_annual_valet
      t.integer :payroll_total_annual_other
      t.integer :payroll_total_annual_owners
      t.string :membership_type
    end
  end
end
