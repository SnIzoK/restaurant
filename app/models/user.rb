class User < ActiveRecord::Base
  attr_accessible *attribute_names

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  attr_accessible :password, :password_confirmation

  include Enumerize

  enumerize :role, in: [:client, :customer_service, :admin], default: :client


  def self.available_membership_types
    Hash[[:allied, :casino, :corporate, :individual, :restaurant].map{|type|
      name = I18n.t("forms.apply_for_discount.membership_type.#{type}", raise: true) rescue type
      [type, name]
    }]
  end

  def self.sign_up_params
    User.attribute_names
  end

  def client?
    role.client?
  end

  def customer_service?
    role.customer_service?
  end

  def admin?
    role.admin?
  end

end
