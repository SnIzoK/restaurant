class Company < ActiveRecord::Base
  attr_accessible *attribute_names

  include Cms::TextFields

  boolean_scope :published
  boolean_scope :show_on_file_a_claim, :for_file_a_claim, false
  boolean_scope :show_on_online_billing, :for_online_billing, false
  scope :sort_by_sorting_position, -> { order("sorting_position asc") }
  default_scope do
    sort_by_sorting_position
  end

  image :image

  def links
    properties_field("links")
  end

  def phones
    properties_field("phones")
  end

end
