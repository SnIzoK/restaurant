class SavingsPartner < ActiveRecord::Base
  attr_accessible *attribute_names

  include Cms::TextFields

  boolean_scope :published
  scope :sort_by_sorting_position, -> { order("sorting_position asc") }
  default_scope do
    sort_by_sorting_position
  end

  image :image

  def links
    properties_field("links")
  end

end
