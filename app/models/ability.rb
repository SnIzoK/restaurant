class Ability
  include CanCan::Ability

  def initialize(u)
    if u.admin? || u.customer_service?
      can :access, :rails_admin
      can :dashboard
    end

    if u.admin?
      can :manage, :all
      can :edit, :files
    end

    if u.customer_service?
      can :read, User
      cannot :edit, :files
    end
  end
end
