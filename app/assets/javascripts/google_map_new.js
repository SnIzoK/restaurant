function initialize_google_map() {
    var $map_wrapper = $("#googleMapWrapper")
    if (!$map_wrapper.length){
      return
    }
    var $map = $map_wrapper.children().filter(".map-container")
    // var data_lat_lng = $map_wrapper.attr("data-center").split(",").map(function(a){return parseFloat(a)})
    // var lat_lng = new google.maps.LatLng(data_lat_lng[0], data_lat_lng[1])
    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var isDraggable = w > 640 ? true : false;
    var xCordinate, zoomZoom = null;
    if (w > 1024){
        xCordinate = 36.095659;
        zoomZoom = 16;
        // yCordinate = -115.132615;
    } else {
        xCordinate = 36.101562;
        zoomZoom = 16;
    }


    var lat_lng = new google.maps.LatLng( 36.101804, -115.132615)
    var styles = [
    {
        "featureType": "landscape",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "stylers": [
            {
                "hue": "#00aaff"
            },
            {
                "saturation": -100
            },
            {
                "gamma": 2.15
            },
            {
                "lightness": 12
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 24
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": 57
            }
        ]
    }
];
    var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
    
    
    var mapOptions = {
        zoom: zoomZoom,
        center: new google.maps.LatLng(xCordinate, -115.132615),
        panControl:false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        }, 
        mapTypeControl:false,
        scaleControl:false,
        streetViewControl:false,
        overviewMapControl:false,
        rotateControl:false,
        draggable: true,
        scrollwheel: false,
        mapTypeControlOptions:{
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, "map_style"]
        }
    };
    var map = new google.maps.Map($map[0],
        mapOptions);
    // var image = 'assets/GH-location-icon.png'

    var $marker = $map_wrapper.children().filter(".marker")
    var $tooltip = $map_wrapper.children().filter('.info-window')
    var html_marker = $marker.get(0)

    var map_marker = new RichMarker({
        map: map,
        position: lat_lng,
        draggable: false,
        flat: true,
        anchor: RichMarkerPosition.MIDDLE,
        content: html_marker
    })

    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
}
//google.maps.event.addDomListener(window, 'resize', initialize);
//google.maps.event.addDomListener(window, 'load', initialize)
$window.on("resize load", initialize_google_map)
$document.on("page:load", initialize_google_map)

function open_marker() {
  $(this).addClass("opened");
  return $(this).closest(".map-wrapper").children().filter(".info-window").addClass("opened");
};

function close_marker() {
  $(this).removeClass("opened");
  return $(this).closest(".map-wrapper").find(".info-window").removeClass("opened");
};

function toggle_marker() {
  if ($(this).hasClass("opened")) {
    return close_marker.apply(this);
  } else {
    return open_marker.apply(this);
  }
};

/*
$document.on("click", ".marker-icon", function() {
  var $marker = $(this).closest(".marker");
  return toggle_marker.apply($marker);
});
*/

$document.on("click", ".marker-icon", function (e) {
    var $marker = $(this).closest(".marker");
    var x = e.pageX
    var y = e.pageY
    var $info_window = $(".info-window")
    var h = $info_window.height()
    var w = $info_window.width()
    var map_offset = $marker.closest(".map-wrapper").offset()
    var map_offset_top = map_offset.top
    var map_offset_left = map_offset.left
    x = x - w / 2
    y = y - h / 2
    x = x - map_offset_left
    y = y - map_offset_top
    var transform_value = "translate("+x+"px,"+y+"px)"

    console.log("e: ", e)
    $info_window.css_with_prefixes({
      "transform": transform_value
    })
    return toggle_marker.apply($marker);

  });

$.clickOut(".info-window", function() {
  var $marker = $(this).closest(".map-wrapper").find(".marker");
  return close_marker.apply($marker);
  //$(".info-window").removeClass('opened')
}, {
  except: ".marker .marker-icon"
});