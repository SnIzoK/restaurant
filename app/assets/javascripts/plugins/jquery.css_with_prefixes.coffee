jQuery.fn.css_with_prefixes = (props, value)->
  h = {}
  if typeof(props) == 'string' && typeof(value) == "undefined"
    return $(this).css(props)

  if typeof(props) == 'string' && typeof(value) != "undefined"
    k = props
    props = {}
    props[k] = value
  prefixed_props = ["transform", "transition", "animation"]
  prefixes = ["-o-", "-ms-", "-moz-", "-webkit-"]
  for k, v of props
    if prefixed_props.indexOf(k) >= 0
      for prefix in prefixes
        h[prefix + k] = v
    h[k] = v

  console.log "h: ", h
  $(this).css(h)