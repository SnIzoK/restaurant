class AdminMailer < ApplicationMailer
  default from: ENV["smtp_gmail_user_name"]
  layout 'mailer'

  def receivers(name)
    config_class = "FormConfigs::#{name.classify}".constantize
    to = config_class.first.try(&:emails) || config_class.default_emails
    to
  end

  def new_policy_change_request(req)

  end

  def new_request(obj)
    init_host
    set_admin_root
    @request = obj
    resource_name = @request.class.name.underscore
    @email_title = I18n.t("mailer.#{resource_name}.title")
    mail to: receivers(resource_name), subject: @email_title, template_name: "basic_request"
  end
end
