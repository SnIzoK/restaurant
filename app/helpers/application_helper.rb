module ApplicationHelper
  def cabinet_links(i18n_root)
    [{key: :profile, icon: "ic_person_black_24px"},
      {key: :check_account, icon: "ic_assignment_black_24px"},
      {key: :messages, icon: "email"}
      ].map{|item| item[:url] = send("cabinet_#{item[:key]}_path"); item[:active] = controller_name == "cabinet" && action_name == item[:key].to_s; item[:name] = I18n.t("#{i18n_root}.#{item[:key]}"); item }
  end

  def cabinet_tabs
    cabinet_links("components.cabinet.tabs")  
  end

  def header_menu_links
    cabinet_links("components.header.menu.account-menu")
  end
end
