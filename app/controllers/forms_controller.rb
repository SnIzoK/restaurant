class FormsController < ApplicationController
  def policy_change_request
    req = PolicyChangeRequest.create(params[:policy_change_request])
    req.notify_admin
  end
end