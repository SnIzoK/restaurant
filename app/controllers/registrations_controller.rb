class RegistrationsController < Users::RegistrationsController
  skip_all_before_action_callbacks

  def create
    User.create(params[:user])

    render json: {}
  end
end