class PagesController < ApplicationController
  before_action :set_page_instance, except: [:index, :online_billing]

	def index
    set_page_metadata(:home)
	end

  def about

  end

  def membership

  end

  def policy_service_center

  end

  def file_a_claim
    @companies = Company.published.for_file_a_claim
  end

  def online_billing
    set_page_metadata(:online_billing_and_payments)
    @companies = Company.published.for_online_billing
  end

  def privacy_policy
    
  end

  def states_eligible
    
  end

  def contacts
    
  end

  def savings_calculator
    @savings_partners = SavingsPartner.published
  end

  def policy_change_request
    
  end

  def cheack_account
    
  end

  private

  def set_page_instance
    set_page_metadata(action_name)
  end

end