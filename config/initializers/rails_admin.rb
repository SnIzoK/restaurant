RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
    nestable do
      only [SavingsPartner, Company]
    end
  end

  config.navigation_static_links = {
      locales: "/file_editor/locales"
  }

  config.include_models Cms::Page, Cms::MetaTags, User, PolicyChangeRequest, SavingsPartner, Company

  config.model Cms::Page do

    list do
      field :name do
        def value
          @bindings[:object].name
        end
      end
    end

    edit do
      field :seo_tags
    end
  end

  config.model Cms::MetaTags do
    visible false
    field :title
    field :keywords
    field :description
  end

  config.model User do
    edit do
      group :basic do
        field :fein
        field :email
        field :password
        field :password_confirmation
        field :role
      end

      group :profile do
        field :first_name
        field :last_name
        field :country
        field :language
        field :membership_type
      end

      group :insurance do
        field :company_name
        field :company_website
        field :company_ages
        field :full_time_employees
        field :part_time_employees
        field :dba
        field :phone
        field :fax
        field :payroll_total_annual_restaurant
        field :payroll_total_annual_clerical
        field :payroll_total_annual_sales
        field :payroll_total_annual_valet
        field :payroll_total_annual_other
        field :payroll_total_annual_owners
      end

    end
  end

  config.model PolicyChangeRequest do
    group :general_info do
      field :user
      field :first_name
      field :last_name
      field :city
      field :street_address
      field :zip_code
      field :state
      field :phone
      field :email
    end

    group :current_insurance_information do
      field :company_name
      field :policy_number
      field :policy_expiration_date
      field :changes_effect_date
      field :changes_description
    end
  end

  config.model SavingsPartner do
    nestable_list({position_field: :sorting_position})

    field :published
    field :name
    field :short_description
    field :main_url
    field :links
    field :saving
    field :saving_subtext
    field :tooltip_title
    field :tooltip_description
    field :image
  end

  config.model Company do
    nestable_list({position_field: :sorting_position})

    field :published
    field :show_on_online_billing
    field :show_on_file_a_claim
    field :name
    field :address
    field :company_site
    field :links
    field :phones
    field :image
  end

end
