Rails.application.routes.draw do

  scope "cabinet", controller: "cabinet" do
    get "profile", action: "profile", as: :cabinet_profile
    get "check_account", action: "check_account", as: :cabinet_check_account
    get "messages", action: "messages", as: :cabinet_messages
  end
  post "policy_change_request", to: "forms#policy_change_request"

  mount Cms::Engine => '/'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  post "sign-up", to: "registrations#create", as: "sign_up"

  devise_for :users, module: :users, path: "", path_names: {
      sign_in: "login",
      sign_out: 'logout',
      sign_up: "sign-up",
      edit: 'profile'
  }
  root to: "pages#index"

  controller "pages" do
    get "membership", action: "membership"
    get "about", action: "about"
    get "discounts", action: "discounts"
    get "policy_service_center", action: "policy_service_center"
    get "file_a_claim", action: "file_a_claim"
    get "online_billing", action: "online_billing"
    get "privacy_policy", action: "privacy_policy"
    get "states_eligible", action: "states_eligible"
    get "contacts", action: "contacts"
    get "savings_calculator", action: "savings_calculator"
    get "policy_change_request", action: "policy_change_request"
    get "cheack_account", action: "cheack_account"
  end
end
